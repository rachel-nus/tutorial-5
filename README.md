# README

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up?

To initialise the database, run:

- ensure that mongo is running locally
- to test: `node api/scripts/trymongo.js`
- run command: `mongo tutorial-5 api/scripts/init.mongo.js`

In one terminal, run:

- `cd api`
- `npm install`
- `npm start`

In another terminal, run:

- `cd ui`
- `npm install`
- `npm start`
