const { MongoClient } = require("mongodb");

const url = "mongodb://localhost/tutorial-5";

function testWithCallbacks(callback) {
  console.log("\n--- testWithCallbacks ---");
  const client = new MongoClient(url, { useNewUrlParser: true });
  client.connect(function (err, client) {
    if (err) {
      callback(err);
      return;
    }
    console.log("Connected to MongoDB");

    const db = client.db();
    const collection = db.collection("customers");

    const customer = {
      id: 999,
      customerName: "mickey mouse",
      phoneNumber: 84889687,
      createdTime: new Date(Date.now()),
    };

    collection.insertOne(customer, function (err, result) {
      if (err) {
        client.close();
        callback(err);
        return;
      }
      console.log("Result of insert:\n", result.insertedId);
      collection.find({ _id: result.insertedId }).toArray(function (err, docs) {
        if (err) {
          client.close();
          callback(err);
          return;
        }
        console.log("Result of find:\n", docs);
        client.close();
        callback(err);
      });
    });

    collection.deleteOne(customer, function (err, result) {
      if (err) {
        client.close();
        callback(err);
        return;
      }
      console.log("Number of records deleted:\n", result.deletedCount);
    });
  });
}

async function testWithAsync() {
  console.log("\n--- testWithAsync ---");
  const client = new MongoClient(url, { useNewUrlParser: true });
  try {
    await client.connect();
    console.log("Connected to MongoDB");
    const db = client.db();
    const collection = db.collection("customers");

    const customer = {
      id: 9999,
      customerName: "mickey mouse",
      phoneNumber: 84889687,
      createdTime: new Date(Date.now()),
    };
    const result = await collection.insertOne(customer);
    console.log("Result of insert:\n", result.insertedId);

    const docs = await collection.find({ _id: result.insertedId }).toArray();
    console.log("Result of find:\n", docs);

    const result2 = await collection.deleteOne(customer);
    console.log("Number of records deleted:\n", result2.deletedCount);
  } catch (err) {
    console.log(err);
  } finally {
    client.close();
  }
}

testWithCallbacks(function (err) {
  if (err) {
    console.log(err);
  }
  testWithAsync();
});
