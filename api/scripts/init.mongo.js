db.customers.remove({});

const customersDB = [
  {
    id: 1,
    customerName: "astra",
    phoneNumber: 84889687,
    createdTime: new Date(Date.now()),
  },
  {
    id: 2,
    customerName: "sova",
    phoneNumber: 84172637,
    createdTime: new Date(Date.now()),
  },
  {
    id: 3,
    customerName: "killjoy",
    phoneNumber: 98472631,
    createdTime: new Date(Date.now()),
  },
  {
    id: 4,
    customerName: "brimstone",
    phoneNumber: 98472631,
    createdTime: new Date(Date.now()),
  },
  {
    id: 5,
    customerName: "kayo",
    phoneNumber: 98472631,
    createdTime: new Date(Date.now()),
  },
];

db.customers.insertMany(customersDB);
const count = db.customers.count();
print("Inserted", count, "customers");

const queueLen = customersDB.length;

db.counters.insert({ _id: "customers", current: queueLen });
