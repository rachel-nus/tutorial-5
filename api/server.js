const fs = require("fs");
const express = require("express");
const { ApolloServer, UserInputError } = require("apollo-server-express");
const { GraphQLScalarType } = require("graphql");
const { Kind } = require("graphql/language");
const { MongoClient } = require("mongodb");

const url = "mongodb://localhost/tutorial-5";
let db;

let aboutMessage = "Issue Tracker API v1.0";

async function getNextSequence(name) {
  const result = await db
    .collection("counters")
    .findOneAndUpdate(
      { _id: name },
      { $inc: { current: 1 } },
      { returnOriginal: false }
    );
  return result.value.current;
}

const GraphQLDate = new GraphQLScalarType({
  name: "GraphQLDate",
  description: "A Date() type in GraphQL as a scalar",
  serialize(value) {
    return value.toLocaleString();
  },
  parseValue(value) {
    const dateValue = new Date(value);
    return isNaN(dateValue) ? undefined : dateValue;
  },
  parseLiteral(ast) {
    if (ast.kind == Kind.STRING) {
      const value = new Date(ast.value);
      return isNaN(value) ? undefined : value;
    }
  },
});

const resolvers = {
  Query: {
    about: () => aboutMessage,
    customersList,
    maxSN,
    queueLen,
  },
  Mutation: {
    setAboutMessage,
    customerAdd,
    customerDelete,
    customerClear,
  },
  GraphQLDate,
};

async function customersList() {
  const customers = await db.collection("customers").find({}).toArray();
  return customers;
}

async function maxSN() {
  const result = await db
    .collection("counters")
    .find({ _id: "customers" })
    .toArray();
  return result[0].current;
}

async function queueLen() {
  return db.collection("customers").countDocuments();
}

function setAboutMessage(_, { message }) {
  return (aboutMessage = message);
}

async function customerAdd(_, { customer }) {
  customer.id = await getNextSequence("customers");
  db.collection("customers").insertOne(customer);
  return customer;
}

function customerDelete(_, { customer }) {
  db.collection("customers").deleteOne({
    id: customer.id,
    customerName: customer.customerName,
  });
  return customer;
}

function customerClear() {
  return db.remove({});
}

async function connectToDb() {
  const client = new MongoClient(url, { useNewUrlParser: true });
  await client.connect();
  console.log("Connected to MongoDB at", url);
  db = client.db();
}

const server = new ApolloServer({
  typeDefs: fs.readFileSync("./schema.graphql", "utf-8"),
  resolvers,
  formatError: (error) => {
    console.log(error);
    return error;
  },
});

const app = express();

app.use(express.static("public"));

server.applyMiddleware({ app, path: "/graphql" });

(async function () {
  try {
    await connectToDb();
    app.listen(5000, function () {
      console.log("App started on port 5000");
    });
  } catch (err) {
    console.log("ERROR:", err);
  }
})();
