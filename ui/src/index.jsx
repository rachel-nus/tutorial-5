// Defaults
const MAX_WAITING = 25;
const defaultState = {
  customers: [],
  maxSn: 0,
  numFreeSlots: MAX_WAITING,
  showWaitlist: false,
};

const pinkContainerClassName =
  "w3-container w3-padding-16 w3-pale-red w3-center w3-wide";
const genericButtonClassName =
  "w3-button w3-round w3-red w3-opacity w3-hover-opacity-off";
const closeButtonClassName =
  "w3-button w3-round w3-green w3-opacity w3-hover-opacity-off";
const deleteButtonClassName =
  "w3-button w3-round w3-grey w3-opacity w3-hover-opacity-off";

const dateRegex = new RegExp("^\\d\\d\\d\\d-\\d\\d-\\d\\d");

function jsonDateReviver(key, value) {
  if (dateRegex.test(value)) return new Date(value);
  return value;
}

class AddCustomer extends React.Component {
  constructor() {
    super();
    this.state = { showForm: false };
    this.hideForm = this.hideForm.bind(this);
  }
  hideForm() {
    this.setState({ showForm: !this.state.showForm });
  }
  render() {
    const showFormStatus = this.state.showForm;
    return (
      <div className={pinkContainerClassName} id="addCustomer">
        <button
          className={
            showFormStatus ? closeButtonClassName : genericButtonClassName
          }
          onClick={() => this.hideForm()}
        >
          {showFormStatus ? "Close" : "Add Customer"}
        </button>
        {showFormStatus && (
          <CustomerForm
            createCustomerOnClick={this.props.createCustomerOnClick}
            numFreeSlots={this.props.numFreeSlots}
          />
        )}
      </div>
    );
  }
}

class CustomerForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      customerName: "",
      phoneNumber: "",
      createdTime: new Date(),
      formErrors: {
        firstError: "",
        secondError: "",
      },
      customerNameIsValid: false,
      phoneNumberIsValid: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.customerNameIsValid & this.state.phoneNumberIsValid) {
      if (this.props.numFreeSlots > 0) {
        const currentDate = new Date(Date.now());
        const customer = {
          customerName: this.state.customerName,
          phoneNumber: this.state.phoneNumber,
          createdTime: currentDate,
        };
        this.props.createCustomerOnClick(customer);
        this.setState({ customerName: "", phoneNumber: "" });
      } else {
        window.alert("sorry, there's no more space left on the waiting list!");
      }
    } else {
      window.alert("please fix the form first!");
    }
  }

  handleNameChange(e) {
    const customerNameInput = e.target.value;
    let fieldValidationErrors = this.state.formErrors;
    if (!/^[a-zA-Z ]+$/.test(customerNameInput)) {
      fieldValidationErrors.firstError =
        "customer name should only contain letters!";
      this.setState({ customerNameIsValid: false });
    } else {
      fieldValidationErrors.firstError = "";
      this.setState({ customerNameIsValid: true });
    }
    this.setState({ customerName: customerNameInput });
  }
  handlePhoneNumberChange(e) {
    const phoneNumberInput = e.target.value;
    let fieldValidationErrors = this.state.formErrors;
    if (!/^\d+$/.test(phoneNumberInput)) {
      fieldValidationErrors.secondError =
        "phone number should only contain numbers!";
      this.setState({ phoneNumberIsValid: false });
    } else if (phoneNumberInput.length != 8) {
      fieldValidationErrors.secondError =
        "phone number should be 8 numbers long!";
      this.setState({ phoneNumberIsValid: false });
    } else {
      fieldValidationErrors.secondError = "";
      this.setState({ phoneNumberIsValid: true });
    }
    this.setState({ phoneNumber: phoneNumberInput });
  }
  render() {
    return (
      <div className={pinkContainerClassName}>
        <form name="addCustForm" onSubmit={this.handleSubmit}>
          <label htmlFor="custName">
            <b> Name:</b>
          </label>
          <input
            type="text"
            id="custName"
            name="custName"
            onChange={this.handleNameChange}
            value={this.state.customerName}
          />
          <div style={{ color: "red" }}>{this.state.formErrors.firstError}</div>
          <br />
          <br />
          <label htmlFor="custPhone">
            <b> Phone Number:</b>
          </label>
          <input
            type="text"
            id="custPhone"
            name="custPhone"
            onChange={this.handlePhoneNumberChange}
            value={this.state.phoneNumber}
          />
          <div style={{ color: "red" }}>
            {this.state.formErrors.secondError}
          </div>
          <br />
          <br />
          <button className={genericButtonClassName}>
            Add Customer to Waitlist
          </button>
        </form>
      </div>
    );
  }
}

class DeleteCustomer extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(e) {
    const customer = this.props.customer;
    this.props.deleteCustomerOnClick(customer);
  }
  render() {
    return (
      <button className={deleteButtonClassName} onClick={this.handleSubmit}>
        <i className="fa fa-close"></i>
      </button>
    );
  }
}

class DisplayCustomers extends React.Component {
  render() {
    const showWaitlist = this.props.showWaitlistStatus;
    return (
      <div className={pinkContainerClassName} id="viewWaitlist">
        {showWaitlist && (
          <div>
            <h3>The Waitlist</h3>
            <WaitListTable
              customers={this.props.customers}
              deleteCustomerOnClick={this.props.deleteCustomerOnClick}
            />
          </div>
        )}
        <button
          className={
            showWaitlist ? closeButtonClassName : genericButtonClassName
          }
          onClick={() => this.props.showCustomersOnClick()}
        >
          {showWaitlist ? "Return to Homepage" : "View Waitlist"}
        </button>
      </div>
    );
  }
}

class DisplayFreeSlots extends React.Component {
  render() {
    return (
      <div className={pinkContainerClassName}>
        Number of Free Slots: {this.props.numFreeSlots}
      </div>
    );
  }
}

class Header extends React.Component {
  render() {
    return (
      <div id="homepage">
        <div className="w3-container w3-padding-16 w3-pale-blue w3-center w3-wide">
          <h1 className="w3-text-grey">
            <b>Hotel California Waitlist System</b>
          </h1>
        </div>
      </div>
    );
  }
}

class DisplayHomepage extends React.Component {
  render() {
    return (
      <div id="homepage">
        <Header />
        <br />
        <DisplayFreeSlots numFreeSlots={this.props.numFreeSlots} />
        <br />
        <AddCustomer
          createCustomerOnClick={this.props.createCustomerOnClick}
          numFreeSlots={this.props.numFreeSlots}
        />
        <br />
        <DisplayCustomers
          customers={this.props.customers}
          showCustomersOnClick={this.props.showCustomersOnClick}
          deleteCustomerOnClick={this.props.deleteCustomerOnClick}
          showWaitlistStatus={this.props.showWaitlistStatus}
        />
        <br />
      </div>
    );
  }
}

function WaitListRow(props) {
  const customerProp = props.customer;
  return (
    <tr>
      <td>{customerProp.id}</td>
      <td>{customerProp.customerName}</td>
      <td>{customerProp.phoneNumber}</td>
      <td>{customerProp.createdTime}</td>
      <td>
        <DeleteCustomer
          customer={customerProp}
          deleteCustomerOnClick={props.deleteCustomerOnClick}
        />
      </td>
    </tr>
  );
}

class WaitListTable extends React.Component {
  render() {
    const customers = this.props.customers;
    const waitListRows = customers.map((customer) => (
      <WaitListRow
        key={customer.id}
        customer={customer}
        deleteCustomerOnClick={this.props.deleteCustomerOnClick}
      />
    ));
    return (
      <div className={pinkContainerClassName}>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Customer Name</th>
              <th>Phone Number</th>
              <th>Created Time</th>
            </tr>
          </thead>
          <tbody>{waitListRows}</tbody>
        </table>
      </div>
    );
  }
}

class SidebarNav extends React.Component {
  render() {
    return (
      <div id="sidebarNav" className="w3-sidebar w3-light-grey w3-bar-block">
        <h3 className="w3-bar-item">
          <b>☰ Menu</b>
        </h3>
        <a href="#homepage" className="w3-bar-item w3-button">
          Homepage
        </a>
        <a href="#addCustomer" className="w3-bar-item w3-button">
          Add Customer
        </a>
        <a href="#viewWaitlist" className="w3-bar-item w3-button">
          View Waitlist
        </a>
      </div>
    );
  }
}

async function graphQLFetch(query, variables = {}) {
  try {
    const response = await fetch("http://localhost:5000/graphql", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ query, variables }),
    });
    const body = await response.text();
    const result = JSON.parse(body, jsonDateReviver);

    if (result.errors) {
      const error = result.errors[0];
      if (error.extensions.code == "BAD_USER_INPUT") {
        const details = error.extensions.exception.errors.join("\n ");
        alert(`${error.message}:\n ${details}`);
      } else {
        alert(`${error.extensions.code}: ${error.message}`);
      }
    }
    return result.data;
  } catch (e) {
    alert(`Error in sending data to server: ${e.message}`);
  }
}

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      numFreeSlots: MAX_WAITING,
      showWaitlist: false,
    };
    this.handleCreateCustomer = this.handleCreateCustomer.bind(this);
    this.handleDeleteCustomer = this.handleDeleteCustomer.bind(this);
    this.handleShowCustomers = this.handleShowCustomers.bind(this);
  }

  componentDidMount() {
    this.loadData();
  }

  async loadData() {
    const query = `query {
      customersList {
        id customerName phoneNumber createdTime
      }
    }`;
    const query2 = `query {
      maxSN 
    }`;
    const query3 = `query {
      queueLen 
    }`;

    const data = await graphQLFetch(query);
    const data2 = await graphQLFetch(query2);
    const data3 = await graphQLFetch(query3);
    if (data) {
      this.setState({ customers: data.customersList });
    }
    if (data2) {
      this.setState({ maxSN: data2.maxSN });
    }
    if (data3) {
      this.setState({ numFreeSlots: MAX_WAITING - data3.queueLen });
    }
  }

  async createCustomer(customer) {
    const query = `mutation customerAdd($customer: CustomerInputs!) {
      customerAdd(customer: $customer) {
        id customerName phoneNumber createdTime
      }
    }`;

    const data = await graphQLFetch(query, { customer });
    if (data) {
      this.loadData();
    }
  }

  async deleteCustomer(customer) {
    delete customer.createdTime;
    const query = `mutation customerDelete($customer: CustomerInputs!) {
      customerDelete(customer: $customer) 
    }`;

    const data = await graphQLFetch(query, { customer });
    if (data) {
      this.loadData();
    }
  }

  handleCreateCustomer(customer) {
    this.createCustomer(customer);
  }

  handleDeleteCustomer(customer) {
    this.deleteCustomer(customer);
  }

  handleShowCustomers() {
    this.setState({ showWaitlist: !this.state.showWaitlist });
  }

  render() {
    return (
      <React.Fragment>
        <div className="bgimg">
          <SidebarNav />
          <div id="pageContent">
            {!this.state.showWaitlist && (
              <div>
                <DisplayHomepage
                  customers={this.state.customers}
                  numFreeSlots={this.state.numFreeSlots}
                  createCustomerOnClick={this.handleCreateCustomer}
                  showCustomersOnClick={this.handleShowCustomers}
                  deleteCustomerOnClick={this.handleDeleteCustomer}
                  showWaitlist={this.state.showWaitlist}
                />
              </div>
            )}
            {this.state.showWaitlist && (
              <div>
                <Header />
                <br />
                <DisplayCustomers
                  customers={this.state.customers}
                  showCustomersOnClick={this.handleShowCustomers}
                  deleteCustomerOnClick={this.handleDeleteCustomer}
                  showWaitlistStatus={this.state.showWaitlist}
                />
              </div>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const element = <App />;
ReactDOM.render(element, document.getElementById("contents"));
