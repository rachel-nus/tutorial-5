"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

// Defaults
var MAX_WAITING = 25;
var defaultState = {
  customers: [],
  maxSn: 0,
  numFreeSlots: MAX_WAITING,
  showWaitlist: false
};
var pinkContainerClassName = "w3-container w3-padding-16 w3-pale-red w3-center w3-wide";
var genericButtonClassName = "w3-button w3-round w3-red w3-opacity w3-hover-opacity-off";
var closeButtonClassName = "w3-button w3-round w3-green w3-opacity w3-hover-opacity-off";
var deleteButtonClassName = "w3-button w3-round w3-grey w3-opacity w3-hover-opacity-off";
var dateRegex = new RegExp("^\\d\\d\\d\\d-\\d\\d-\\d\\d");

function jsonDateReviver(key, value) {
  if (dateRegex.test(value)) return new Date(value);
  return value;
}

var AddCustomer = /*#__PURE__*/function (_React$Component) {
  _inherits(AddCustomer, _React$Component);

  var _super = _createSuper(AddCustomer);

  function AddCustomer() {
    var _this;

    _classCallCheck(this, AddCustomer);

    _this = _super.call(this);
    _this.state = {
      showForm: false
    };
    _this.hideForm = _this.hideForm.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(AddCustomer, [{
    key: "hideForm",
    value: function hideForm() {
      this.setState({
        showForm: !this.state.showForm
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var showFormStatus = this.state.showForm;
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName,
        id: "addCustomer"
      }, /*#__PURE__*/React.createElement("button", {
        className: showFormStatus ? closeButtonClassName : genericButtonClassName,
        onClick: function onClick() {
          return _this2.hideForm();
        }
      }, showFormStatus ? "Close" : "Add Customer"), showFormStatus && /*#__PURE__*/React.createElement(CustomerForm, {
        createCustomerOnClick: this.props.createCustomerOnClick,
        numFreeSlots: this.props.numFreeSlots
      }));
    }
  }]);

  return AddCustomer;
}(React.Component);

var CustomerForm = /*#__PURE__*/function (_React$Component2) {
  _inherits(CustomerForm, _React$Component2);

  var _super2 = _createSuper(CustomerForm);

  function CustomerForm(props) {
    var _this3;

    _classCallCheck(this, CustomerForm);

    _this3 = _super2.call(this, props);
    _this3.state = {
      customerName: "",
      phoneNumber: "",
      createdTime: new Date(),
      formErrors: {
        firstError: "",
        secondError: ""
      },
      customerNameIsValid: false,
      phoneNumberIsValid: false
    };
    _this3.handleSubmit = _this3.handleSubmit.bind(_assertThisInitialized(_this3));
    _this3.handleNameChange = _this3.handleNameChange.bind(_assertThisInitialized(_this3));
    _this3.handlePhoneNumberChange = _this3.handlePhoneNumberChange.bind(_assertThisInitialized(_this3));
    return _this3;
  }

  _createClass(CustomerForm, [{
    key: "handleSubmit",
    value: function handleSubmit(e) {
      e.preventDefault();

      if (this.state.customerNameIsValid & this.state.phoneNumberIsValid) {
        if (this.props.numFreeSlots > 0) {
          var currentDate = new Date(Date.now());
          var customer = {
            customerName: this.state.customerName,
            phoneNumber: this.state.phoneNumber,
            createdTime: currentDate
          };
          this.props.createCustomerOnClick(customer);
          this.setState({
            customerName: "",
            phoneNumber: ""
          });
        } else {
          window.alert("sorry, there's no more space left on the waiting list!");
        }
      } else {
        window.alert("please fix the form first!");
      }
    }
  }, {
    key: "handleNameChange",
    value: function handleNameChange(e) {
      var customerNameInput = e.target.value;
      var fieldValidationErrors = this.state.formErrors;

      if (!/^[a-zA-Z ]+$/.test(customerNameInput)) {
        fieldValidationErrors.firstError = "customer name should only contain letters!";
        this.setState({
          customerNameIsValid: false
        });
      } else {
        fieldValidationErrors.firstError = "";
        this.setState({
          customerNameIsValid: true
        });
      }

      this.setState({
        customerName: customerNameInput
      });
    }
  }, {
    key: "handlePhoneNumberChange",
    value: function handlePhoneNumberChange(e) {
      var phoneNumberInput = e.target.value;
      var fieldValidationErrors = this.state.formErrors;

      if (!/^\d+$/.test(phoneNumberInput)) {
        fieldValidationErrors.secondError = "phone number should only contain numbers!";
        this.setState({
          phoneNumberIsValid: false
        });
      } else if (phoneNumberInput.length != 8) {
        fieldValidationErrors.secondError = "phone number should be 8 numbers long!";
        this.setState({
          phoneNumberIsValid: false
        });
      } else {
        fieldValidationErrors.secondError = "";
        this.setState({
          phoneNumberIsValid: true
        });
      }

      this.setState({
        phoneNumber: phoneNumberInput
      });
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName
      }, /*#__PURE__*/React.createElement("form", {
        name: "addCustForm",
        onSubmit: this.handleSubmit
      }, /*#__PURE__*/React.createElement("label", {
        htmlFor: "custName"
      }, /*#__PURE__*/React.createElement("b", null, " Name:")), /*#__PURE__*/React.createElement("input", {
        type: "text",
        id: "custName",
        name: "custName",
        onChange: this.handleNameChange,
        value: this.state.customerName
      }), /*#__PURE__*/React.createElement("div", {
        style: {
          color: "red"
        }
      }, this.state.formErrors.firstError), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("label", {
        htmlFor: "custPhone"
      }, /*#__PURE__*/React.createElement("b", null, " Phone Number:")), /*#__PURE__*/React.createElement("input", {
        type: "text",
        id: "custPhone",
        name: "custPhone",
        onChange: this.handlePhoneNumberChange,
        value: this.state.phoneNumber
      }), /*#__PURE__*/React.createElement("div", {
        style: {
          color: "red"
        }
      }, this.state.formErrors.secondError), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("button", {
        className: genericButtonClassName
      }, "Add Customer to Waitlist")));
    }
  }]);

  return CustomerForm;
}(React.Component);

var DeleteCustomer = /*#__PURE__*/function (_React$Component3) {
  _inherits(DeleteCustomer, _React$Component3);

  var _super3 = _createSuper(DeleteCustomer);

  function DeleteCustomer(props) {
    var _this4;

    _classCallCheck(this, DeleteCustomer);

    _this4 = _super3.call(this, props);
    _this4.handleSubmit = _this4.handleSubmit.bind(_assertThisInitialized(_this4));
    return _this4;
  }

  _createClass(DeleteCustomer, [{
    key: "handleSubmit",
    value: function handleSubmit(e) {
      var customer = this.props.customer;
      this.props.deleteCustomerOnClick(customer);
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("button", {
        className: deleteButtonClassName,
        onClick: this.handleSubmit
      }, /*#__PURE__*/React.createElement("i", {
        className: "fa fa-close"
      }));
    }
  }]);

  return DeleteCustomer;
}(React.Component);

var DisplayCustomers = /*#__PURE__*/function (_React$Component4) {
  _inherits(DisplayCustomers, _React$Component4);

  var _super4 = _createSuper(DisplayCustomers);

  function DisplayCustomers() {
    _classCallCheck(this, DisplayCustomers);

    return _super4.apply(this, arguments);
  }

  _createClass(DisplayCustomers, [{
    key: "render",
    value: function render() {
      var _this5 = this;

      var showWaitlist = this.props.showWaitlistStatus;
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName,
        id: "viewWaitlist"
      }, showWaitlist && /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h3", null, "The Waitlist"), /*#__PURE__*/React.createElement(WaitListTable, {
        customers: this.props.customers,
        deleteCustomerOnClick: this.props.deleteCustomerOnClick
      })), /*#__PURE__*/React.createElement("button", {
        className: showWaitlist ? closeButtonClassName : genericButtonClassName,
        onClick: function onClick() {
          return _this5.props.showCustomersOnClick();
        }
      }, showWaitlist ? "Return to Homepage" : "View Waitlist"));
    }
  }]);

  return DisplayCustomers;
}(React.Component);

var DisplayFreeSlots = /*#__PURE__*/function (_React$Component5) {
  _inherits(DisplayFreeSlots, _React$Component5);

  var _super5 = _createSuper(DisplayFreeSlots);

  function DisplayFreeSlots() {
    _classCallCheck(this, DisplayFreeSlots);

    return _super5.apply(this, arguments);
  }

  _createClass(DisplayFreeSlots, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName
      }, "Number of Free Slots: ", this.props.numFreeSlots);
    }
  }]);

  return DisplayFreeSlots;
}(React.Component);

var Header = /*#__PURE__*/function (_React$Component6) {
  _inherits(Header, _React$Component6);

  var _super6 = _createSuper(Header);

  function Header() {
    _classCallCheck(this, Header);

    return _super6.apply(this, arguments);
  }

  _createClass(Header, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        id: "homepage"
      }, /*#__PURE__*/React.createElement("div", {
        className: "w3-container w3-padding-16 w3-pale-blue w3-center w3-wide"
      }, /*#__PURE__*/React.createElement("h1", {
        className: "w3-text-grey"
      }, /*#__PURE__*/React.createElement("b", null, "Hotel California Waitlist System"))));
    }
  }]);

  return Header;
}(React.Component);

var DisplayHomepage = /*#__PURE__*/function (_React$Component7) {
  _inherits(DisplayHomepage, _React$Component7);

  var _super7 = _createSuper(DisplayHomepage);

  function DisplayHomepage() {
    _classCallCheck(this, DisplayHomepage);

    return _super7.apply(this, arguments);
  }

  _createClass(DisplayHomepage, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        id: "homepage"
      }, /*#__PURE__*/React.createElement(Header, null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(DisplayFreeSlots, {
        numFreeSlots: this.props.numFreeSlots
      }), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(AddCustomer, {
        createCustomerOnClick: this.props.createCustomerOnClick,
        numFreeSlots: this.props.numFreeSlots
      }), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(DisplayCustomers, {
        customers: this.props.customers,
        showCustomersOnClick: this.props.showCustomersOnClick,
        deleteCustomerOnClick: this.props.deleteCustomerOnClick,
        showWaitlistStatus: this.props.showWaitlistStatus
      }), /*#__PURE__*/React.createElement("br", null));
    }
  }]);

  return DisplayHomepage;
}(React.Component);

function WaitListRow(props) {
  var customerProp = props.customer;
  return /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, customerProp.id), /*#__PURE__*/React.createElement("td", null, customerProp.customerName), /*#__PURE__*/React.createElement("td", null, customerProp.phoneNumber), /*#__PURE__*/React.createElement("td", null, customerProp.createdTime), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement(DeleteCustomer, {
    customer: customerProp,
    deleteCustomerOnClick: props.deleteCustomerOnClick
  })));
}

var WaitListTable = /*#__PURE__*/function (_React$Component8) {
  _inherits(WaitListTable, _React$Component8);

  var _super8 = _createSuper(WaitListTable);

  function WaitListTable() {
    _classCallCheck(this, WaitListTable);

    return _super8.apply(this, arguments);
  }

  _createClass(WaitListTable, [{
    key: "render",
    value: function render() {
      var _this6 = this;

      var customers = this.props.customers;
      var waitListRows = customers.map(function (customer) {
        return /*#__PURE__*/React.createElement(WaitListRow, {
          key: customer.id,
          customer: customer,
          deleteCustomerOnClick: _this6.props.deleteCustomerOnClick
        });
      });
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName
      }, /*#__PURE__*/React.createElement("table", null, /*#__PURE__*/React.createElement("thead", null, /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("th", null, "ID"), /*#__PURE__*/React.createElement("th", null, "Customer Name"), /*#__PURE__*/React.createElement("th", null, "Phone Number"), /*#__PURE__*/React.createElement("th", null, "Created Time"))), /*#__PURE__*/React.createElement("tbody", null, waitListRows)));
    }
  }]);

  return WaitListTable;
}(React.Component);

var SidebarNav = /*#__PURE__*/function (_React$Component9) {
  _inherits(SidebarNav, _React$Component9);

  var _super9 = _createSuper(SidebarNav);

  function SidebarNav() {
    _classCallCheck(this, SidebarNav);

    return _super9.apply(this, arguments);
  }

  _createClass(SidebarNav, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        id: "sidebarNav",
        className: "w3-sidebar w3-light-grey w3-bar-block"
      }, /*#__PURE__*/React.createElement("h3", {
        className: "w3-bar-item"
      }, /*#__PURE__*/React.createElement("b", null, "\u2630 Menu")), /*#__PURE__*/React.createElement("a", {
        href: "#homepage",
        className: "w3-bar-item w3-button"
      }, "Homepage"), /*#__PURE__*/React.createElement("a", {
        href: "#addCustomer",
        className: "w3-bar-item w3-button"
      }, "Add Customer"), /*#__PURE__*/React.createElement("a", {
        href: "#viewWaitlist",
        className: "w3-bar-item w3-button"
      }, "View Waitlist"));
    }
  }]);

  return SidebarNav;
}(React.Component);

function graphQLFetch(_x) {
  return _graphQLFetch.apply(this, arguments);
}

function _graphQLFetch() {
  _graphQLFetch = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(query) {
    var variables,
        response,
        body,
        result,
        error,
        details,
        _args4 = arguments;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            variables = _args4.length > 1 && _args4[1] !== undefined ? _args4[1] : {};
            _context4.prev = 1;
            _context4.next = 4;
            return fetch("http://localhost:5000/graphql", {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify({
                query: query,
                variables: variables
              })
            });

          case 4:
            response = _context4.sent;
            _context4.next = 7;
            return response.text();

          case 7:
            body = _context4.sent;
            result = JSON.parse(body, jsonDateReviver);

            if (result.errors) {
              error = result.errors[0];

              if (error.extensions.code == "BAD_USER_INPUT") {
                details = error.extensions.exception.errors.join("\n ");
                alert("".concat(error.message, ":\n ").concat(details));
              } else {
                alert("".concat(error.extensions.code, ": ").concat(error.message));
              }
            }

            return _context4.abrupt("return", result.data);

          case 13:
            _context4.prev = 13;
            _context4.t0 = _context4["catch"](1);
            alert("Error in sending data to server: ".concat(_context4.t0.message));

          case 16:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[1, 13]]);
  }));
  return _graphQLFetch.apply(this, arguments);
}

var App = /*#__PURE__*/function (_React$Component10) {
  _inherits(App, _React$Component10);

  var _super10 = _createSuper(App);

  function App() {
    var _this7;

    _classCallCheck(this, App);

    _this7 = _super10.call(this);
    _this7.state = {
      numFreeSlots: MAX_WAITING,
      showWaitlist: false
    };
    _this7.handleCreateCustomer = _this7.handleCreateCustomer.bind(_assertThisInitialized(_this7));
    _this7.handleDeleteCustomer = _this7.handleDeleteCustomer.bind(_assertThisInitialized(_this7));
    _this7.handleShowCustomers = _this7.handleShowCustomers.bind(_assertThisInitialized(_this7));
    return _this7;
  }

  _createClass(App, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.loadData();
    }
  }, {
    key: "loadData",
    value: function () {
      var _loadData = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var query, query2, query3, data, data2, data3;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                query = "query {\n      customersList {\n        id customerName phoneNumber createdTime\n      }\n    }";
                query2 = "query {\n      maxSN \n    }";
                query3 = "query {\n      queueLen \n    }";
                _context.next = 5;
                return graphQLFetch(query);

              case 5:
                data = _context.sent;
                _context.next = 8;
                return graphQLFetch(query2);

              case 8:
                data2 = _context.sent;
                _context.next = 11;
                return graphQLFetch(query3);

              case 11:
                data3 = _context.sent;

                if (data) {
                  this.setState({
                    customers: data.customersList
                  });
                }

                if (data2) {
                  this.setState({
                    maxSN: data2.maxSN
                  });
                }

                if (data3) {
                  this.setState({
                    numFreeSlots: MAX_WAITING - data3.queueLen
                  });
                }

              case 15:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function loadData() {
        return _loadData.apply(this, arguments);
      }

      return loadData;
    }()
  }, {
    key: "createCustomer",
    value: function () {
      var _createCustomer = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(customer) {
        var query, data;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                console.log("createcustomer ", customer);
                query = "mutation customerAdd($customer: CustomerInputs!) {\n      customerAdd(customer: $customer) {\n        id customerName phoneNumber createdTime\n      }\n    }";
                _context2.next = 4;
                return graphQLFetch(query, {
                  customer: customer
                });

              case 4:
                data = _context2.sent;

                if (data) {
                  this.loadData();
                }

              case 6:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function createCustomer(_x2) {
        return _createCustomer.apply(this, arguments);
      }

      return createCustomer;
    }()
  }, {
    key: "deleteCustomer",
    value: function () {
      var _deleteCustomer = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(customer) {
        var query, data;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                console.log("deletecustomer ", customer);
                delete customer.createdTime;
                query = "mutation customerDelete($customer: CustomerInputs!) {\n      customerDelete(customer: $customer) \n    }";
                console.log("deletecustomer query", query);
                _context3.next = 6;
                return graphQLFetch(query, {
                  customer: customer
                });

              case 6:
                data = _context3.sent;

                if (data) {
                  this.loadData();
                }

              case 8:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function deleteCustomer(_x3) {
        return _deleteCustomer.apply(this, arguments);
      }

      return deleteCustomer;
    }()
  }, {
    key: "handleCreateCustomer",
    value: function handleCreateCustomer(customer) {
      console.log("handlecreatecustomer", customer);
      this.createCustomer(customer);
    }
  }, {
    key: "handleDeleteCustomer",
    value: function handleDeleteCustomer(customer) {
      console.log("handledeleteCustomer", customer);
      this.deleteCustomer(customer);
    }
  }, {
    key: "handleShowCustomers",
    value: function handleShowCustomers() {
      this.setState({
        showWaitlist: !this.state.showWaitlist
      });
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
        className: "bgimg"
      }, /*#__PURE__*/React.createElement(SidebarNav, null), /*#__PURE__*/React.createElement("div", {
        id: "pageContent"
      }, !this.state.showWaitlist && /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(DisplayHomepage, {
        customers: this.state.customers,
        numFreeSlots: this.state.numFreeSlots,
        createCustomerOnClick: this.handleCreateCustomer,
        showCustomersOnClick: this.handleShowCustomers,
        deleteCustomerOnClick: this.handleDeleteCustomer,
        showWaitlist: this.state.showWaitlist
      })), this.state.showWaitlist && /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Header, null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(DisplayCustomers, {
        customers: this.state.customers,
        showCustomersOnClick: this.handleShowCustomers,
        deleteCustomerOnClick: this.handleDeleteCustomer,
        showWaitlistStatus: this.state.showWaitlist
      })))));
    }
  }]);

  return App;
}(React.Component);

var element = /*#__PURE__*/React.createElement(App, null);
ReactDOM.render(element, document.getElementById("contents"));